package odf;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stax.StAXResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class OdfHistory {

	public static void create(Path historyFile, Path repoDir, Path officeDir,
			OdfNormalizer odfNormalizer) throws UtilException,
			InterruptedException, IOException, SAXException,
			TransformerException {

		LinkedList<OdfHistoryEntry> files = OdfHistoryParser.parse(historyFile,
				odfNormalizer);
		initGit(repoDir);

		Queue<WorkerItem> queue = new LinkedList<WorkerItem>();

		for (OdfHistoryEntry file : files) {
			System.out.println(file.path);
			Path inzip = officeDir.resolve(file.path);
			Path outzip = repoDir.resolve("odt").resolve(file.path);
			queue.add(new WorkerItem(inzip, outzip));
		}

		LinkedList<Worker> workers = new LinkedList<Worker>();
		final int cores = Runtime.getRuntime().availableProcessors();
		for (int i = 0; i < cores; i++) {
			Worker worker = new Worker(queue);
			worker.start();
			workers.add(worker);
		}
		for (Worker worker : workers) {
			worker.join();
		}

		for (OdfHistoryEntry file : files) {
			final Path odtpath = repoDir.resolve("odt").resolve(file.path);
			if (file.dir != null) {
				Path dir = repoDir.resolve(file.dir);
				// remove files from a previous commit if there are any
				// retain all files ending in '.rng'
				if (Files.exists(dir)) {
					Utils.removeRecursive(dir, ".rng");
				}
				Unzip.unzip(odtpath, dir);
			} else {
				final Path target = repoDir.resolve(file.target);
				Files.createDirectories(target.getParent());
				Files.copy(odtpath, target, StandardCopyOption.REPLACE_EXISTING);
			}

			Utils.cmd(repoDir, "git", "add", "-A");
			Utils.cmd(repoDir, "git", "commit", "-m", file.path + " "
					+ file.date);
		}
	}

	static private void initGit(Path repoDir) throws IOException,
			UtilException, InterruptedException {
		if (Files.exists(repoDir)) {
			Utils.removeContents(repoDir);
		} else {
			Files.createDirectories(repoDir);
		}
		Utils.cmd(repoDir, "git", "init");
		Files.createDirectories(repoDir.resolve("odt"));
		Files.write(repoDir.resolve(".gitignore"), "odt/\n".getBytes());
		Utils.cmd(repoDir, "git", "add", "-A");
		Utils.cmd(repoDir, "git", "commit", "-m", "setup");
	}

	static private XMLOutputFactory factory = XMLOutputFactory.newInstance();

	static void pretty(Document document, OutputStream outputStream,
			HashSet<QName> elementsWithNoText) throws TransformerException,
			XMLStreamException {

		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
		transformer.setOutputProperty(OutputKeys.INDENT, "no");
		Source source = new DOMSource(document);
		XMLStreamWriter writer = factory.createXMLStreamWriter(outputStream,
				"UTF-8");
		StAXResult result = new StAXResult(new PrettyPrinter(writer,
				elementsWithNoText));
		transformer.transform(source, result);
	}
}

class WorkerItem {
	final Path src;
	final Path target;

	WorkerItem(Path src, Path target) {
		this.src = src;
		this.target = target;
	}
}

class Worker extends Thread {
	private final Queue<WorkerItem> queue;
	private final OdfZipNormalizer n;

	Worker(Queue<WorkerItem> queue) {
		this.queue = queue;
		n = new OdfZipNormalizer();
	}

	@Override
	public void run() {
		while (true) {
			WorkerItem item = null;

			synchronized (queue) {
				if (queue.isEmpty()) {
					return;
				}
				item = queue.remove();
			}

			try {
				convert(item);
			} catch (Exception e) {
				System.err.println("Error normalizing file " + item.src);
				System.err.println(e.getClass().getName() + " "
						+ e.getMessage());
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

	private void convert(WorkerItem item) throws IOException, SAXException,
			TransformerException, NormalizeException, XMLStreamException {
		System.out.println(item.src);
		n.normalize(item.src, item.target);
	}

}