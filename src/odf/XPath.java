package odf;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XPath {

	private static final XPathFactory xPathfactory = XPathFactory.newInstance();
	private static final javax.xml.xpath.XPath xpath;

	static {
		xpath = xPathfactory.newXPath();
		xpath.setNamespaceContext(new NC());
	}

	static public NodeList eval(Node n, String xp) {
		try {
			XPathExpression x = xpath.compile(xp);
			return (NodeList) x.evaluate(n, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new RuntimeException(e);
		}
	}

	static public XPathResult<Element> elementIterator(Node n, String xpath) {
		return new XPathResult<Element>(eval(n, xpath),
				new NodeConverter<Element>() {
					public Element convert(Node n) {
						return (Element) n;
					}
				});
	}

	static public XPathResult<Attr> attrIterator(Node n, String xpath) {
		return new XPathResult<Attr>(eval(n, xpath), new NodeConverter<Attr>() {
			public Attr convert(Node n) {
				return (Attr) n;
			}
		});
	}

	static public XPathResult<Node> nodeIterator(Node n, String xpath) {
		return new XPathResult<Node>(eval(n, xpath), new NodeConverter<Node>() {
			public Node convert(Node n) {
				return n;
			}
		});
	}

	static public XPathResult<String> stringIterator(Node n, String xpath) {
		return new XPathResult<String>(eval(n, xpath),
				new NodeConverter<String>() {
					public String convert(Node n) {
						if (n instanceof Element) {
							Element e = (Element) n;
							return e.getTextContent();
						}
						return n.getNodeValue();
					}
				});
	}
}

class XPathResult<T> implements Iterable<T> {
	final NodeList nodes;
	final NodeConverter<T> converter;

	XPathResult(NodeList nodes, NodeConverter<T> converter) {
		this.nodes = nodes;
		this.converter = converter;
	}

	@Override
	public Iterator<T> iterator() {
		return new NodeIterator<T>(nodes, converter);
	}

	public int size() {
		return nodes.getLength();
	}

	public Object[] toArray() {
		final int l = nodes.getLength();
		Object a[] = new Object[l];
		for (int i = 0; i < l; ++i) {
			a[i] = nodes.item(i);
		}
		return a;
	}

	public T get(int i) {
		return converter.convert(nodes.item(i));
	}
}

interface NodeConverter<T> {
	T convert(Node n);
}

class NodeIterator<T> implements Iterator<T> {
	final NodeList nodes;
	int position = 0;
	final NodeConverter<T> converter;

	NodeIterator(NodeList nodes, NodeConverter<T> converter) {
		this.nodes = nodes;
		this.converter = converter;
	}

	@Override
	public boolean hasNext() {
		return position < nodes.getLength();
	}

	@Override
	public T next() {
		if (!hasNext()) {
			throw new NoSuchElementException();
		}
		return converter.convert(nodes.item(position++));
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}

class NC implements NamespaceContext {

	public final static String chart = "urn:oasis:names:tc:opendocument:xmlns:chart:1.0";
	public final static String config = "urn:oasis:names:tc:opendocument:xmlns:config:1.0";
	public final static String db = "urn:oasis:names:tc:opendocument:xmlns:database:1.0";
	public final static String dc = "http://purl.org/dc/elements/1.1/";
	public final static String dr3d = "urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0";
	public final static String draw = "urn:oasis:names:tc:opendocument:xmlns:drawing:1.0";
	public final static String fo = "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0";
	public final static String form = "urn:oasis:names:tc:opendocument:xmlns:form:1.0";
	public final static String manifest = "urn:oasis:names:tc:opendocument:xmlns:manifest:1.0";
	public final static String math = "http://www.w3.org/1998/Math/MathML";
	public final static String meta = "urn:oasis:names:tc:opendocument:xmlns:meta:1.0";
	public final static String number = "urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0";
	public final static String office = "urn:oasis:names:tc:opendocument:xmlns:office:1.0";
	public final static String presentation = "urn:oasis:names:tc:opendocument:xmlns:presentation:1.0";
	public final static String rng = "http://relaxng.org/ns/structure/1.0";
	public final static String style = "urn:oasis:names:tc:opendocument:xmlns:style:1.0";
	public final static String svg = "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0";
	public final static String table = "urn:oasis:names:tc:opendocument:xmlns:table:1.0";
	public final static String text = "urn:oasis:names:tc:opendocument:xmlns:text:1.0";
	public final static String xforms = "http://www.w3.org/2002/xforms";
	public final static String xlink = "http://www.w3.org/1999/xlink";
	public final static String xml = "http://www.w3.org/XML/1998/namespace";
	public final static String xmlns = "http://www.w3.org/2000/xmlns/";

	private static final Map<String, String> p;
	private static final Map<String, String> n;
	static {
		// use reflection to create the maps
		p = new TreeMap<String, String>();
		n = new TreeMap<String, String>();
		final Field[] fields = NC.class.getDeclaredFields();
		for (Field f : fields) {
			final int modifiers = f.getModifiers();
			if (f.getType().equals(String.class)
					&& Modifier.isStatic(modifiers)
					&& Modifier.isFinal(modifiers)
					&& Modifier.isPublic(modifiers)) {
				try {
					final String prefix = f.getName();
					final String uri = (String) f.get(null);
					p.put(prefix, uri);
					n.put(uri, prefix);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

	@Override
	public String getNamespaceURI(@Nullable String prefix) {
		return p.get(prefix);
	}

	@Override
	public @Nullable String getPrefix(@Nullable String namespaceURI) {
		return n.get(namespaceURI);
	}

	@Override
	public Iterator<String> getPrefixes(@Nullable String namespaceURI) {
		List<String> l = new LinkedList<String>();
		String prefix = getPrefix(namespaceURI);
		if (prefix != null) {
			l.add(prefix);
		}
		return l.iterator();
	}

}
