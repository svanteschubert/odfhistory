package odf;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.sax.SAXSource;

import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class XML {

	final private SAXParser parser;

	final static private SAXParserFactory saxParserFactory = SAXParserFactory
			.newInstance();

	static {
		saxParserFactory.setValidating(false);
		saxParserFactory.setNamespaceAware(true);
		try {
			saxParserFactory
					.setFeature(
							"http://apache.org/xml/features/nonvalidating/load-dtd-grammar",
							false);
			saxParserFactory
					.setFeature(
							"http://apache.org/xml/features/nonvalidating/load-external-dtd",
							false);
			saxParserFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING,
					true);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	XML() {
		try {
			parser = saxParserFactory.newSAXParser();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	Matcher whitespaceMatcher = Pattern.compile("^[ \t\r\n]*$").matcher("");

	boolean isWhitespace(Node n) {
		if (n.getNodeType() == Node.TEXT_NODE) {
			whitespaceMatcher.reset(n.getNodeValue());
			return whitespaceMatcher.matches();
		}
		return false;
	}

	public Document parse(InputStream in, @Nullable OdfInfo info,
			@Nullable StyleNames styleNames) throws IOException {
		Document doc;
		try {
			XMLReader reader = parser.getXMLReader();
			DOMResult domResult = new DOMResult();
			if (info != null) {
				WhitespaceHandler whitespace = new WhitespaceHandler(
						info.getRngInfo(this));
				whitespace.setParent(reader);

				final UnitHandler unit = new UnitHandler(info.getRngInfo(this));
				unit.setParent(whitespace);
				PrefixHandler prefix = new PrefixHandler();
				prefix.setParent(unit);
				OdfFixer f = new OdfFixer(info.version, styleNames);
				f.setParent(prefix);
				reader = f;
			}

			Transformer transformer = TransformerFactory.newInstance()
					.newTransformer();
			transformer.transform(new SAXSource(reader, new InputSource(in)),
					domResult);
			in.close();
			doc = (Document) domResult.getNode();
			doc.setXmlStandalone(true);
		} catch (SAXException | TransformerException e) {
			throw new IOException(e);
		}
		return doc;
	}
}
