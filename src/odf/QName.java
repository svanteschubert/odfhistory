package odf;

import java.util.HashMap;

import org.eclipse.jdt.annotation.Nullable;

public class QName implements Comparable<QName> {
	static HashMap<String, HashMap<String, QName>> qnames = new HashMap<String, HashMap<String, QName>>();
	static int counter = 0;
	final int id;
	final String namespaceURI;
	final String localName;

	@Override
	public int compareTo(@Nullable QName o) {
		if (o == this) {
			return 0;
		}
		if (o == null) {
			return -1;
		}
		int c = namespaceURI.compareTo(o.namespaceURI);
		if (c != 0) {
			return c;
		}
		return localName.compareTo(o.localName);
	}

	private QName(String namespaceURI, String localName) {
		this.id = counter++;
		this.namespaceURI = namespaceURI;
		this.localName = localName;
	}

	static QName qname(@Nullable String namespaceURI, String localName) {
		String ns = (namespaceURI == null) ? "" : namespaceURI;
		HashMap<String, QName> m = qnames.get(ns);
		if (m == null) {
			m = new HashMap<String, QName>();
			qnames.put(ns, m);
		}
		QName qname = m.get(localName);
		if (qname == null) {
			qname = new QName(ns, localName);
			m.put(localName, qname);
		}
		return qname;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return this;
	}

	@Override
	public boolean equals(@Nullable Object obj) {
		return obj == this;
	}

	@Override
	public String toString() {
		if ("".equals(namespaceURI)) {
			return localName;
		}
		return "{" + namespaceURI + "}" + localName;
	}

	@Override
	public int hashCode() {
		return id;
	}
}
